type Status = 'Successful' | 'Pending'
type Type = 'Water' | 'Electricity'
type StoreExp = {
  id: number
  type: Type
  status: Status
  total: number
  duedate: string
}

export type { Type, Status, StoreExp }
