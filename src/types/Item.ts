import type { Product } from './Product'

type Item = {
  id: number
  name: string
  // size: string
  // mood: string
  // sweet: string
  unit: number
  price: number
  productId: number
  product: Product
}
export { type Item }
