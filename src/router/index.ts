import { createRouter, createWebHistory } from 'vue-router'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/stats',
      name: 'stats',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      components: {
        default: () => import('../views/Stats.vue'),
        menu: () => import('../components/MainNavigation.vue'),
        header: () => import('../components/MainAppbar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/um',
      name: 'um',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      components: {
        default: () => import('../views/UserManagementView.vue'),
        menu: () => import('../components/MainNavigation.vue'),
        header: () => import('../components/MainAppbar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/sm',
      name: 'sm',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      components: {
        default: () => import('../views/StockManagementView.vue'),
        menu: () => import('../components/MainNavigation.vue'),
        header: () => import('../components/MainAppbar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/cm',
      name: 'cm',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      components: {
        default: () => import('../views/CustomerManagementView.vue'),
        menu: () => import('../components/MainNavigation.vue'),
        header: () => import('../components/MainAppbar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/em',
      name: 'em',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      components: {
        default: () => import('../views/EmployeeManagementView.vue'),
        menu: () => import('../components/MainNavigation.vue'),
        header: () => import('../components/MainAppbar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    // {
    //   path: '/products',
    //   name: 'products',
    //   // route level code-splitting
    //   // this generates a separate chunk (About.[hash].js) for this route
    //   // which is lazy-loaded when the route is visited.
    //   components: {
    //     default: () => import('../views/ProductsView.vue'),
    //     menu: () => import('../components/MainNavigation.vue'),
    //     header: () => import('../components/MainAppbar.vue')
    //   },
    //   meta: {
    //     layout: 'MainLayout',
    //     requireAuth: true
    //   }
    // },
    {
      path: '/pos',
      name: 'pos',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      components: {
        default: () => import('../views/POSView.vue'),
        menu: () => import('../components/MainNavigation.vue'),
        header: () => import('../components/MainAppbar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/cico',
      name: 'cico',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      components: {
        default: () => import('../views/CiCoView.vue'),
        menu: () => import('../components/MainNavigation.vue'),
        header: () => import('../components/MainAppbar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/cicotable',
      name: 'cicotable',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      components: {
        default: () => import('../views/CiCoTable.vue'),
        menu: () => import('../components/MainNavigation.vue'),
        header: () => import('../components/MainAppbar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/se',
      name: 'se',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      components: {
        default: () => import('../views/StoreExpensesView.vue'),
        menu: () => import('../components/MainNavigation.vue'),
        header: () => import('../components/MainAppbar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/sal',
      name: 'sal',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      components: {
        default: () => import('../views/SalariesView.vue'),
        menu: () => import('../components/MainNavigation.vue'),
        header: () => import('../components/MainAppbar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/login',
      name: 'login',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('../views/LoginView.vue'),
      meta: {
        layout: 'FullLayout',
        requireAuth: false
      }
    }
  ]
})
function isLogin() {
  const user = localStorage.getItem('user')
  if (user) {
    return true
  }
  return false
}
router.beforeEach((to, from) => {
  console.log(from)
  console.log(to)
  if (to.meta.requireAuth && !isLogin()) {
    router.replace('/login')
  }
})
export default router
