import http from './http'
import type { Salary } from '@/types/Salary'
function addSalary(Salary: Salary) {
  return http.post('/Salarys', Salary)
}

function editSalary(Salary: Salary) {
  return http.patch(`/Salarys/${Salary.id}`, Salary)
}

function delSalary(Salary: Salary) {
  return http.delete(`/Salarys/${Salary.id}`)
}

function getSalary(id: number) {
  return http.get(`/Salarys/${id}`)
}

function getSalarys() {
  return http.get('/Salarys')
}
export default { addSalary, editSalary, delSalary, getSalary, getSalarys }
